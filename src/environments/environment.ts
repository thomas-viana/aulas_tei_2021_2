// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// import { config } from "process";

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCyGaQfAXUl040Y144wcsvmut6uPXICWF8",
    authDomain: "ifinancas-controle-ifsp-gru.firebaseapp.com",
    projectId: "ifinancas-controle-ifsp-gru",
    storageBucket: "ifinancas-controle-ifsp-gru.appspot.com",
    messagingSenderId: "357922418788",
    appId: "1:357922418788:web:d52284411e3cded9c92f71",
    measurementId: "G-2PTVP3Z84K"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
