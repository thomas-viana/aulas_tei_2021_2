import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculadoraService {
  
  constructor() { }

  divide(dividendo, divisor): number | string{
      if(divisor == 0){
          return 'Não existe divisão por zero!!!'
      }
      return dividendo / divisor;
  }

  multiplica(multiplicar1, multiplicar2): number | string{
      if(multiplicar2 == 0){
          return 'Não existe multiplicação por zero!!!'
      }
      return multiplicar1 * multiplicar2;
  }

  subtrai(subtrair1, subtrair2): number | string{
      if(subtrair2 == 0){
          return 'Não existe subtração por zero!!!'
      }
      return subtrair1 - subtrair2;
  }

  soma(somar1, somar2): number | string{
      var valorTotal;

      if(somar2 == 0){
          return 'Não existe somar por zero!!!'
      }else{
        valorTotal = Number(somar1) + Number(somar2);
      }
      return valorTotal;
  }

}
