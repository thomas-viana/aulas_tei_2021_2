import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';

import { CalculadoraPage } from './calculadora.page';


describe('A página Calculadora', () => {
  let view;
  let model: CalculadoraPage;
  let fixture: ComponentFixture<CalculadoraPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CalculadoraPage],
      imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(CalculadoraPage);
    model = fixture.componentInstance;
    view = fixture.nativeElement;
    fixture.detectChanges();
  }));

  it('deve ter um título', () => {
      expect(model.titulo).toBeDefined();
  });

  it('deve renderizar o título', () => {
    const result = view.querySelector('.title').textContent;

    expect(result).toEqual('Calculadora');
  });

  it('deve estar com o botão desabilitado', () => {
      expect(model.form.invalid).toBeTrue();
  });

  it('divide após o clique no botão', () =>{
      //arrange
      model.form.controls.num1.setValue(20);
      model.form.controls.num2.setValue(4);
      
      //act
      const operacao = fixture.debugElement.query(By.css('#divider'));
      operacao.triggerEventHandler('click', null);
      fixture.detectChanges();

      //assert
      const quociente = view.querySelector('#quociente');
      expect(quociente.textContent).toEqual('5');
  });

  it('multiplica após o clique no botão', () =>{
    //arrange
    model.form.controls.num1.setValue(6);
    model.form.controls.num2.setValue(6);
    
    //act
    const operacao = fixture.debugElement.query(By.css('#multiplicar'));
    operacao.triggerEventHandler('click', null);
    fixture.detectChanges();

    //assert
    const quociente = view.querySelector('#quociente');
    expect(quociente.textContent).toEqual('36');
});

it('subtrai após o clique no botão', () =>{
    //arrange
    model.form.controls.num1.setValue(10);
    model.form.controls.num2.setValue(5);
    
    //act
    const operacao = fixture.debugElement.query(By.css('#subtrair'));
    operacao.triggerEventHandler('click', null);
    fixture.detectChanges();

    //assert
    const quociente = view.querySelector('#quociente');
    expect(quociente.textContent).toEqual('5');
});

it('soma após o clique no botão', () =>{
    //arrange
    model.form.controls.num1.setValue(10);
    model.form.controls.num2.setValue(13);
    
    //act
    const operacao = fixture.debugElement.query(By.css('#somar'));
    operacao.triggerEventHandler('click', null);
    fixture.detectChanges();

    //assert
    const quociente = view.querySelector('#quociente');
    expect(quociente.textContent).toEqual('23');
});

});
