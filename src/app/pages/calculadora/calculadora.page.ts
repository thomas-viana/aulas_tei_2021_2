import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CalculadoraService } from 'src/app/services/calculadora.service';


@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.page.html',
  styleUrls: ['./calculadora.page.scss'],
})
export class CalculadoraPage implements OnInit {
  titulo = 'Calculadora';
  form: FormGroup;
  quociente: string | number;

  constructor(
      private builder: FormBuilder,
      private calculadora: CalculadoraService
  ) { }
      
  ngOnInit() {
    this.form = this.builder.group({
      num1: ['', [Validators.required, Validators.min(1)]],
      num2: ['', [Validators.required, Validators.min(1)]]
    });
  }

  dividir() {
    const data = this.form.value;
    const dividendo = data.num1;
    const divisor = data.num2;

    this.quociente = this.calculadora.divide(dividendo, divisor);
  }

  multiplicar(){
    const data = this.form.value;
    const multiplicar1 = data.num1;
    const multiplicar2 = data.num2;

    this.quociente = this.calculadora.multiplica(multiplicar1, multiplicar2);
  }

  subtrair(){
    const data = this.form.value;
    const subtrair1 = data.num1;
    const subtrair2 = data.num2;

    this.quociente = this.calculadora.subtrai(subtrair1, subtrair2);
  }

  somar(){
    const data = this.form.value;
    const somar1 = data.num1;
    const somar2 = data.num2;

    this.quociente = this.calculadora.soma(somar1, somar2);
  }

}