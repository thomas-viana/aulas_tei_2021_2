import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, ToastController } from '@ionic/angular';
import { DateHelper } from 'src/app/helpers/DateHelper';
import { ContaService } from '../service/conta-service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  contasForm: FormGroup;

  constructor(
    private builder : FormBuilder,
    private nav : NavController,
    private toast : ToastController,
    private conta : ContaService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm(){
    this.contasForm = this.builder.group({
      tipo : ['',[Validators.required]],
      date : [new Date().toISOString(), Validators.required],
      valor : ['', [Validators.required, Validators.min(0.01)]],
      parceiro : ['', [Validators.required, Validators.minLength(6)]],
      descricao: ['', [Validators.required, Validators.minLength(6)]]
    })
  }

  registrarConta(){
    let conta = this.contasForm.value;
    const date = this.contasForm.get('date').value;
    conta = {...conta, ...DateHelper.breakDate(date)};
    delete conta.date; 

    this.conta.registrarConta(conta)
    .then(() => this.nav.navigateBack('home'));
  }
}
