import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, AlertController } from '@ionic/angular';
import { ContaService } from '../service/conta-service';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.page.html',
  styleUrls: ['./lista.page.scss'],
})
export class ListaPage implements OnInit {
  listaContas;
  tipo;

  constructor(
    private conta: ContaService,
    private nav: NavController,
    private alert: AlertController,
    private router: Router
  ) { }

  ngOnInit() {
    const tipo = this.router.url.split('/')
    this.tipo = tipo[2].charAt(0).toUpperCase()+tipo[2].slice(1)
    this.conta.lista(tipo[2]).subscribe(x => this.listaContas = x)
  }

  async remove(conta){
    const confirm = await this.alert.create({
      header: 'Remover Conta',
      message: 'Deseja realmente excluir esta conta?',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel'
      },
      {
        text: 'Deletar',
        handler: () => this.conta.remove(conta)
      }]
    });

    confirm.present();
  }

  async edita(conta) {
    const confirm = await this.alert.create({
      cssClass: 'editarPopup',
      header: 'Editar os dados da conta' + this.tipo,
      inputs: [{
        type: 'text', name: 'parceiro', label: 'Parceiro', value: conta.parceiro
      }, {
        type: 'text', name: 'descricao', label: 'Descrição', value: conta.descricao
      },{
        type: 'number', name: 'valor', label: 'Valor', value: conta.valor
      }],
      buttons: [{
        text: 'Cancelar',
        role: 'cancel'
      }, {
        text: 'Editar',
        handler: (data) => {
          const obj = {...conta, ...data};
          this.conta.edita(obj);
        }
      }]
    });
    confirm.present();
  }
  
}
