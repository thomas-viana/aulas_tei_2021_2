import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/compat/firestore';
import { ToastController } from '@ionic/angular';
import { DateHelper } from 'src/app/helpers/DateHelper';

@Injectable({
  providedIn: 'root'
})
export class ContaService {
  collection : AngularFirestoreCollection;

  constructor(
    private db : AngularFirestore,
    private toast: ToastController
  ) { }

  registrarConta(conta){
    conta.id = this.db.createId();
    this.collection = this.db.collection('conta');
    return this.collection.doc(conta.id).set(conta).then(() => this.presentToast());
  }

  async presentToast() {
    const toast = await this.toast.create({
      message: 'Conta salva',
        duration: 2000
    });
    toast.present();
  }
  
  lista(tipo){
    this.collection = this.db.collection('conta', ref => ref.where('tipo', '==', tipo));
    return this.collection.valueChanges();
  }

  remove(conta){
    this.collection = this.db.collection('conta');
    this.collection.doc(conta.id).delete();
  }

  edita(conta){
    const dados = conta
    dados.tipo = conta.tipo
    this.collection = this.db.collection('conta');
    this.collection.doc(conta.id).update(dados);
    
  }
  /**
   * Totaliza as contas de acordo com seu tipo
   * @para tipo: string - modalidade de contas
   */
  /**
   * Totaliza as contas de acordo com seu tipo
   * @para tipo: string - modalidade de contas
   */
   total(tipo, date){
    const aux = DateHelper.breakDate(date);
    const ano = aux.ano;
    const mes = aux.mes;

    this.collection = this.db.collection('conta', ref => 
    ref.where('tipo', '==', tipo).where('mes', '==', mes).where('ano', '==', ano));
    return this.collection.get().pipe(map(snap => {
      let cont = 0;
      let sum = 0;

      snap.docs.map(doc =>{
        const conta = doc.data();
        const valor = parseFloat(conta.valor);
        sum += valor;
        cont++;
      });

      return { num: cont, valor: sum };
    }));
  }
  
}

